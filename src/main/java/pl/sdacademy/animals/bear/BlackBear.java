package pl.sdacademy.animals.bear;

import pl.sdacademy.animals.time.Clock;

import java.time.LocalDate;

public class BlackBear extends Bear {
    public BlackBear(int weight, Clock clock) {
        super(weight, clock);
    }

    public BlackBear(int weight) {
        super(weight);
    }

    @Override
    public boolean isHibernating() {
        LocalDate currentDate = clock.getCurrentDate();
        LocalDate nineteenthNovember = currentDate
                .withMonth(11)
                .withDayOfMonth(19);
        LocalDate fifteenthMarch = currentDate
                .withMonth(3)
                .withDayOfMonth(15);
        return currentDate.isAfter(nineteenthNovember) ||
                currentDate.isBefore(fifteenthMarch);
    }
}