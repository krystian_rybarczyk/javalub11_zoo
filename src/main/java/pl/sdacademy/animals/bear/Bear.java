package pl.sdacademy.animals.bear;

import pl.sdacademy.animals.Animal;
import pl.sdacademy.animals.time.Clock;
import pl.sdacademy.animals.time.DateTimeClock;


public abstract class Bear implements Animal {

    private double weight;
    private boolean isAlive;
    protected Clock clock;

    public Bear(int weight) {
        this.weight = weight;
        this.isAlive = false;
        this.clock = new DateTimeClock();
    }

    public Bear(int weight, Clock clock) {
        this(weight);
        this.clock = clock;
    }

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    public void eat() {}

    @Override
    public double getWeight() {
        return weight;
    }

    public void drink(int waterWeight) {
        this.weight += waterWeight * 0.75;
    }

    public abstract boolean isHibernating();
}
