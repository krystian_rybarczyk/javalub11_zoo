package pl.sdacademy.animals.bear;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.sdacademy.animals.time.Clock;

import java.time.LocalDate;

public class BearTest {

    @Test
    public void bearShouldNotBeAliveImmediatelyAfterCreation() {
        int weight = 3;
        Bear bear = new BlackBear(weight);

        boolean result = bear.isAlive();

        Assertions.assertFalse(result);
    }

    @Test
    public void bearsWeightShouldIncreaseBy75PercentAfterDrinking() {
        int weight = 44;
        Bear bear = new BlackBear(weight);
        int waterWeight = 3;

        bear.drink(waterWeight);

        Assertions.assertEquals(weight + 0.75 * waterWeight, bear.getWeight(), 0.0005);
    }

    @Test
    public void blackBearShouldBeHibernatingBetween20thNovemberAnd15thMarch() {
        int weight = 44;
        FakeClock fakeClock = new FakeClock();
        LocalDate sixteenthMarch = LocalDate.now()
                .withMonth(11)
                .withDayOfMonth(20);
        Bear bear = new BlackBear(weight, fakeClock);

        fakeClock.setDate(sixteenthMarch);

        Assertions.assertTrue(bear.isHibernating());
    }

    @Test
    public void blackBearShouldNotBeHibernatingBetween16thMarchAnd19thNovember() {
        int weight = 44;
        FakeClock fakeClock = new FakeClock();
        LocalDate sixteenthMarch = LocalDate.now()
                .withMonth(3)
                .withDayOfMonth(16);
        Bear bear = new BlackBear(weight, fakeClock);

        fakeClock.setDate(sixteenthMarch);

        Assertions.assertFalse(bear.isHibernating());
    }

    class FakeClock implements Clock {

        private LocalDate date;

        @Override
        public LocalDate getCurrentDate() {
            return date;
        }

        public void setDate(LocalDate date) {
            this.date = date;
        }
    }
}
